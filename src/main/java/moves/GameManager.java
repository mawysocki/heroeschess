package moves;

import board.Board;
import board.Field;
import pieces.King;
import storage.Icons;

import javax.swing.*;


public class GameManager {

    public static boolean isKingKilled(Field field) {
        return field.getPiece() instanceof King;
    }

    public static void endGame() {
        final String option1 = "Play Again";
        final String option2 = "Play again - reverse sites";
        final String option3 = "Close app";
        final String messageInfo = "Winner: " + Turn.getInstance()
                .getCurrentTurn();
        final String messageHeader = "Game over!";

        Object[] options = {option1,
//                option2,
                option3};
        int result = JOptionPane.showOptionDialog(Board.getInstance(),
                messageInfo,
                messageHeader,
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                Icons.getWinnerIcon(Turn.getInstance().getCurrentTurn()),
                options,
                null);
        selectEndOption(result);

    }

    private static void selectEndOption(int result) {
        switch (result) {
            case 0 -> {
                Board.getInstance().closeBoard();
                Board.newInstance();
                Board.getInstance().start();
                //Stars who lost
                Turn.getInstance().switchTurn();
            }
            case 1 -> Board.getInstance().closeBoard();
            default -> {
            }
        }
    }
}

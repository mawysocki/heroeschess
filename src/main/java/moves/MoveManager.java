package moves;

import board.BoardManager;
import board.Field;
import storage.MonsterFactory;

import java.awt.*;
import java.io.IOException;

public abstract class MoveManager {

    protected Field selectedField = null;
    protected Turn turn;

    public MoveManager() {
        turn = Turn.getInstance();
    }

    protected void resetSelectedField() {
        selectedField = null;
    }

    protected void selectNewItem(Field field) {
        selectedField = field;
        selectedField.setBackground(Color.ORANGE);
    }

    protected void stayOnEmptyField(Field field) {
        field.setPiece(selectedField.getPiece());
        selectedField.clearPiece();
        selectedField.resetBackground();
        resetSelectedField();
        turn.switchTurn();
        BoardManager.resetHighlights();
    }

    protected void unselectItem() {
        resetSelectedField();
        BoardManager.resetHighlights();
    }


    protected void prepareAttack(Field field) {
        if (turn.isOpponent(field)) {
            if (GameManager.isKingKilled(field)) {
                GameManager.endGame();
            } else {
                attack(field);
                turn.switchTurn();
                BoardManager.resetHighlights();
            }

        } else {
            System.out.println("You cannot attack yourself");
        }
    }

    protected void attack(Field field) {
        try {
            field.setPiece(MonsterFactory.getNewMonster(selectedField.getPiece()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        selectedField.clearPiece();
        selectedField.resetBackground();
        resetSelectedField();
    }
}

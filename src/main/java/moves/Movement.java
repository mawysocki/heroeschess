package moves;

import board.Field;

import java.util.List;

public interface Movement {

    List<Field> findAllMoves(Field field);
}

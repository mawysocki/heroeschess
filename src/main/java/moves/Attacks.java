package moves;

import board.Board;
import board.BoardManager;
import board.Field;
import storage.Direction;

import java.util.ArrayList;
import java.util.List;

public class Attacks {

    private final List<Field> allMoves;

    public Attacks() {
        allMoves = new ArrayList<>();
    }

    public Attacks findAttack(Field field, int range, Direction direction) {
        //Pobiera współrzędne zaznaczonego pionka
        int startX = field.getIndexX();
        int startY = field.getIndexY();
        //Sprawdza wszystkie pola o danym wektorze i dopuszczalnym zasięgu
        for (int i = 1; i <= range; i++) {
            //Weryfikowane pole to aktualna pozycja piona plus wektor przemnożony przez zasięg
            int newX = startX + direction.x * i;
            int newY = startY + direction.y * i;

            //Wchodzi do środka jeśli nowy punkt znajduje się na planszy
            //Nie ma elsa
            if (BoardManager.isOnBoard(newX, newY)) {
                //Wchodzi do środka gdy nowe pole jest puste (brak pionka)
                if (Board.getField(newX, newY).isEmpty()) {
                    //Jak pole jest bez pionka to sprawdza czy nie ma naturalnych przeszkód (obiketu Obstacle)
                    if (Board.getField(newX, newY).isClean()) {
                        //Jeśli nie ma to dodaje możliwy ruch do listy i przechodzi do dalszej iteracji
                        allMoves.add(Board.getField(newX, newY));
                    } else {
                        //Jeśli jest przeszkoda to sprawdza czy może coś z nią zrobić
                        //Jeśli blokuje przejście na amen to kończy się iteracja po danym wektorze
                        if (Board.getField(startX, startY).getPiece().getMoveType().isProhibited(Board.getField(newX, newY).getObstacle())) {
                            break;
                        }
                        //Jeśli można ominąć przeszkodę to iteracja jest kontynouwana bez dodawania pola do możliwych ruchów
                        //Jeśli zasięg pozwoli to kolejne pole za przeszkodą może zostać zdobyte

                    }
                } else {
                    //Gdy wie, że to pionek to sprawdza czy to przeciwnik
                    //Jeśli tak to dodaje go do możliwych ataków
                    if (Turn.getInstance().isOpponent(Board.getField(newX, newY))) {
                        allMoves.add(Board.getField(newX, newY));
                    }
                    //Bez względu na to do kogo należy pionek to iteracja sie kończy gdyż nie można omijać pionków
                    break;
                }

            }
        }
        return this;
    }

    public Attacks findAttackAllCross(Field field, int range) {
        return findAttack(field, range, Direction.LEFTUP)
                .findAttack(field, range, Direction.RIGHTUP).
                findAttack(field, range, Direction.LEFTDOWN).
                findAttack(field, range, Direction.RIGHTDOWN);
    }

    public Attacks findAttackAllFlat(Field field, int range) {
        return findAttack(field, range, Direction.UP).
                findAttack(field, range, Direction.LEFT).
                findAttack(field, range, Direction.RIGHT).
                findAttack(field, range, Direction.DOWN);
    }

    public Attacks findAttackAllDirections(Field field, int range) {
        return findAttackAllCross(field, range).
                findAttackAllFlat(field, range);

    }

    public Attacks findAttack2plus1(Field field, int range) {
        return findAttack(field, range, Direction.KNIGHTLEFTUP).
                findAttack(field, range, Direction.KNIGHTUPLEFT).
                findAttack(field, range, Direction.KNIGHTRIGHTUP).
                findAttack(field, range, Direction.KNIGHTUPRIGHT).
                findAttack(field, range, Direction.KNIGHTDOWNLEFT).
                findAttack(field, range, Direction.KNIGHTLEFTDOWN).
                findAttack(field, range, Direction.KNIGHTRIGHTDOWN).
                findAttack(field, range, Direction.KNIGHTDOWNRIGHT);
    }

    public Attacks findAttack3plus1(Field field, int range) {
        return findAttack(field, range, Direction.HUSARLEFTUP).
                findAttack(field, range, Direction.HUSARUPLEFT).
                findAttack(field, range, Direction.HUSARRIGHTUP).
                findAttack(field, range, Direction.HUSARUPRIGHT).
                findAttack(field, range, Direction.HUSARDOWNLEFT).
                findAttack(field, range, Direction.HUSARLEFTDOWN).
                findAttack(field, range, Direction.HUSARRIGHTDOWN).
                findAttack(field, range, Direction.HUSARDOWNRIGHT);
    }

    public List<Field> getAllMoves() {
        return allMoves;
    }


}

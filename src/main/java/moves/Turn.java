package moves;

import board.Field;
import storage.PlayerColor;

public class Turn {
    private PlayerColor currentTurn;

    private static final Turn turn = new Turn();

    private Turn() {
        currentTurn = PlayerColor.WHITE;
    }

    public static Turn getInstance() {
        return turn;
    }

    public PlayerColor getCurrentTurn() {
        return currentTurn;
    }
    //Only for testing purpose
    public boolean isCorrectTurn() {
        return true;
    }

    public boolean isCorrectTurn(Field field) {
        return field.getPiece().getPlayerColor().equals(currentTurn);
    }

    public boolean isOpponent(Field field) {
        return !isCorrectTurn(field);
    }

    public void switchTurn() {
        if (currentTurn.equals(PlayerColor.WHITE)) {
            currentTurn = PlayerColor.BLACK;
        } else {
            currentTurn = PlayerColor.WHITE;
        }
    }

}

package moves;

import board.BoardManager;
import board.Field;
import storage.Obstacle;

import java.util.ArrayList;
import java.util.List;

public class PerformManager extends MoveManager {

    List<Field> moves;

    //Verify if chosen field is in available moves list
    private boolean isAllowedMove(Field field) {
        return moves.contains(field);
    }

    public void performAction(Field field) {
        if (selectedField == null) {
            moves = prepareToSelect(field);
        } else if (isAllowedMove(field)) {
            prepareToMove(field);
        } else {
            if (!field.isClean()) {
                displayWarning(field.getObstacle());
            }
        }

    }

    //Method should return list of all allowed steps for selected field
    private List<Field> prepareToSelect(Field field) {
        List<Field> allowedMoves = new ArrayList<>();
        if (!field.isEmpty() && turn.isCorrectTurn(field)) {
            selectNewItem(field);
            allowedMoves = selectedField.getPiece().findAllMoves(selectedField);

            //Highligh all field which can be attacks
            //Green if fields are empty
            //Red if you attack opponent
            BoardManager.highlightMoveFields(allowedMoves);
            BoardManager.highlightAttackFields(allowedMoves);
            //Selected field must be added to allowed moves because it must be able to unselect itself
            //It must be added after highlight due different color of selected field
            allowedMoves.add(field);
        }
        return allowedMoves;
    }

    //Method is call when clicked field is included in allowed moves list
    //Advanced logic for: empty move, attack and unselect choice
    private void prepareToMove(Field field) {
        //Może stanąć bez walki tylko gdy pole jest puste
        if (field.isEmpty()) {
            //Nie może również zawierać przeszkód
            //lecz przeszkody są elminowane na poziomie generowania ruchu
            stayOnEmptyField(field);
        } else {
            //Jeśli wchodzi na sam siebie to znaczy, że odznacza wybrany element
            if (selectedField.equals(field)) {
                unselectItem();
            } else {
                prepareAttack(field);
            }
        }
    }

    private void displayWarning(Obstacle obstacle) {
        System.out.println(String.format("Watch out, %s!", obstacle));
    }
}

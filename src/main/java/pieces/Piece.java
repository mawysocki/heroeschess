package pieces;

import board.Field;
import moves.Movement;
import storage.Icons;
import storage.MoveType;
import storage.PlayerColor;

import java.awt.*;
import java.io.IOException;

public abstract class Piece implements Movement {
    protected final Image icon;
    protected final String pieceName;

    protected int range = 0;
    protected final PlayerColor playerColor;
    protected final MoveType moveType;

    public Piece(PlayerColor playerColor, MoveType moveType) throws IOException {
        pieceName = extractName();
        this.playerColor = playerColor;
        this.icon = Icons.getIcon(pieceName, playerColor);
        this.moveType = moveType;
        setRange();
    }

    private String extractName() {
        return this.getClass().getName().toLowerCase().replace("pieces.","");
    }
    public Image getIcon() {
        return icon;
    }

    public String getPieceName() {
        return pieceName;
    }
    public PlayerColor getPlayerColor() {
        return playerColor;
    }
    public MoveType getMoveType() {
        return moveType;
    }


    protected abstract void setRange();

}

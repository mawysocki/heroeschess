package pieces;

import storage.PlayerColor;
import java.io.IOException;

public class Draco extends Dragon {

    public Draco(PlayerColor playerColor) throws IOException {
        super(playerColor);
        setRange();
    }

    @Override
    public void setRange() {
        range = 5;
    }

}

package pieces;


import storage.PlayerColor;

import java.io.IOException;


public class Archangel extends Angel {

    public Archangel(PlayerColor playerColor) throws IOException {
        super(playerColor);
        setRange();
    }

    @Override
    public void setRange() {
        range = 5;
    }

}

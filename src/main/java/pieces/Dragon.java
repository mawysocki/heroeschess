package pieces;

import board.Field;
import moves.Attacks;
import storage.MoveType;
import storage.PlayerColor;

import java.io.IOException;
import java.util.List;

public class Dragon extends Piece {

    public Dragon(PlayerColor playerColor) throws IOException {
        super(playerColor, MoveType.FLY);
        setRange();
    }

    @Override
    public void setRange() {
        range = 4;
    }

    @Override
    public List<Field> findAllMoves(Field field) {
        Attacks attacks = new Attacks();
        return attacks.findAttackAllCross(field, range)
                .getAllMoves();
    }
}

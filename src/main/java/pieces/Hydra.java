package pieces;

import board.Board;
import board.BoardManager;
import board.Field;
import moves.Attacks;
import storage.MoveType;
import storage.PlayerColor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Hydra extends Piece {

    public Hydra(PlayerColor playerColor) throws IOException {
        super(playerColor, MoveType.WALK);
        setRange();
    }

    @Override
    public void setRange() {
        range = 3;
    }

    @Override
    public List<Field> findAllMoves(Field field) {
        Attacks attacks = new Attacks();
        return attacks.findAttackAllDirections(field, range)
                .getAllMoves();
    }
}

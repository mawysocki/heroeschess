package pieces;

import storage.PlayerColor;

import java.io.IOException;

public class Pope extends Bishop{
    public Pope(PlayerColor playerColor) throws IOException {
        super(playerColor);
    }

    @Override
    public void setRange() {
        range = 6;
    }
}

package pieces;

import board.Field;
import moves.Attacks;
import storage.MoveType;
import storage.PlayerColor;

import java.io.IOException;
import java.util.List;

public class Bishop extends Piece {

    public Bishop(PlayerColor playerColor) throws IOException {
        super(playerColor, MoveType.WALK);
        setRange();
    }

    @Override
    public void setRange() {
        range = 4;
    }

    @Override
    public List<Field> findAllMoves(Field field) {
        Attacks attacks = new Attacks();
        return attacks.findAttackAllCross(field, range)
                .getAllMoves();
    }
}

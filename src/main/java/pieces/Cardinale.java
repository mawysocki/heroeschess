package pieces;

import storage.PlayerColor;

import java.io.IOException;

public class Cardinale extends Bishop{
    public Cardinale(PlayerColor playerColor) throws IOException {
        super(playerColor);
    }

    @Override
    public void setRange() {
        range = 5;
    }
}

package pieces;

import board.Field;
import moves.Attacks;
import storage.PlayerColor;

import java.io.IOException;
import java.util.List;

public class Titan extends Rook {

    public Titan(PlayerColor playerColor) throws IOException {
        super(playerColor);
        setRange();
    }

    @Override
    public void setRange() {
        range = 5;
    }
}

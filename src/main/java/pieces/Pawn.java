package pieces;

import board.Board;
import board.Field;
import moves.Attacks;
import storage.Direction;
import storage.MoveType;
import storage.PlayerColor;

import java.io.IOException;
import java.util.List;

public class Pawn extends Piece {

    public Pawn(PlayerColor playerColor) throws IOException {
        super(playerColor, MoveType.WALK);
    }

    @Override
    public void setRange() {
        range = 1;
    }

    @Override
    public List<Field> findAllMoves(Field field) {
        Attacks attacks = new Attacks();
        if (playerColor.equals(PlayerColor.WHITE)) {
            return getWhiteAttack(field);

        } else {
            return getBlackAttack(field);
        }

    }
    private List<Field> getWhiteAttack(Field field) {
        Attacks attacks = new Attacks();
        if (!Board.isBoardReversed()) {
            return attacks.findAttack(field, range, Direction.UP)
                    .findAttack(field, range, Direction.LEFTUP)
                    .findAttack(field, range, Direction.RIGHTUP)
                    .getAllMoves();
        } else {
            return attacks.findAttack(field, range, Direction.DOWN)
                    .findAttack(field, range, Direction.LEFTDOWN)
                    .findAttack(field, range, Direction.RIGHTDOWN)
                    .getAllMoves();
        }
    }
    private List<Field> getBlackAttack(Field field) {
        Attacks attacks = new Attacks();
        if (Board.isBoardReversed()) {
            return attacks.findAttack(field, range, Direction.UP)
                    .findAttack(field, range, Direction.LEFTUP)
                    .findAttack(field, range, Direction.RIGHTUP)
                    .getAllMoves();
        } else {
            return attacks.findAttack(field, range, Direction.DOWN)
                    .findAttack(field, range, Direction.LEFTDOWN)
                    .findAttack(field, range, Direction.RIGHTDOWN)
                    .getAllMoves();
        }
    }
}

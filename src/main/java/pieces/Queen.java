package pieces;

import board.Field;
import moves.Attacks;
import storage.MoveType;
import storage.PlayerColor;

import java.io.IOException;
import java.util.List;

public class Queen extends Piece {

    public Queen(PlayerColor playerColor) throws IOException {
        super(playerColor, MoveType.WALK);
        setRange();
    }

    @Override
    public void setRange() {
        range = 8;
    }

    @Override
    public List<Field> findAllMoves(Field field) {
        Attacks attacks = new Attacks();
        return attacks.findAttackAllDirections(field, range)
                .getAllMoves();
    }
}

package pieces;

import board.Field;
import moves.Attacks;
import storage.MoveType;
import storage.PlayerColor;

import java.io.IOException;
import java.util.List;

public class Knight extends Piece {

    public Knight(PlayerColor playerColor) throws IOException {
        super(playerColor, MoveType.WALK);
    }

    @Override
    public void setRange() {
        range = 1;
    }

    @Override
    public List<Field> findAllMoves(Field field) {
        Attacks attacks = new Attacks();
        return attacks.findAttack2plus1(field, range).getAllMoves();

    }
}

import board.Board;

import java.util.Random;

public class Runner {
    public static void main(String[] args) {
        int noOfRandomObstacles = new Random().nextInt(10);
        Board.newInstance(noOfRandomObstacles);
        Board.getInstance().start();
    }
}

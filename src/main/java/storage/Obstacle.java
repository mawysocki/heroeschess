package storage;

import java.awt.*;

public enum Obstacle {

    HOLE(new Color(0, 0, 0), true, false),
    ICE(new Color(255, 255, 0), false, false),
    FIRE(new Color(255, 150, 50), true, true),
    RAIN(new Color(35, 15, 245), false, false),
    STORM(new Color(100, 0, 100), false, true),

    QUICKSAND(new Color(205, 205, 0), false, false),

    WALL(new Color(160, 160, 160), true, true);


    public final Color color;
    public final boolean isWalkBlocker;
    public final boolean isFlyBlocker;

    Obstacle(Color color, boolean isWalkBlocker, boolean isFlyBlocker) {
        this.color = color;
        this.isWalkBlocker = isWalkBlocker;
        this.isFlyBlocker = isFlyBlocker;
    }

}

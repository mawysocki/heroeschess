package storage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum MoveType {

    WALK(Obstacle.HOLE, Obstacle.FIRE, Obstacle.WALL),
    FLY(Obstacle.FIRE, Obstacle.STORM, Obstacle.WALL);

    final List<Obstacle> obstacles;
    MoveType(Obstacle...obstacles){
        this.obstacles = new ArrayList<>();
        this.obstacles.addAll(Arrays.asList(obstacles));
    }

    public boolean isProhibited(Obstacle obstacle) {
        return obstacles.contains(obstacle);
    }
}

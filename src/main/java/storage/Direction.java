package storage;

import java.awt.*;

public enum Direction {
    //Vectors for moves in all directions
    LEFTUP(-1, -1),
    UP(0, -1),
    RIGHTUP(1, -1),
    LEFT(-1, 0),
    RIGHT(1, 0),
    LEFTDOWN(-1, 1),
    DOWN(0, 1),
    RIGHTDOWN(1, 1),

    KNIGHTLEFTUP(-2,-1),
    KNIGHTUPLEFT(-1,-2),
    KNIGHTRIGHTUP(2,-1),
    KNIGHTUPRIGHT(1,-2),
    KNIGHTLEFTDOWN(-2,1),
    KNIGHTDOWNLEFT(-1,2),
    KNIGHTRIGHTDOWN(2,1),
    KNIGHTDOWNRIGHT(1,2),

    HUSARLEFTUP(-3,-1),
    HUSARUPLEFT(-1,-3),
    HUSARRIGHTUP(3,-1),
    HUSARUPRIGHT(1,-3),
    HUSARLEFTDOWN(-3,1),
    HUSARDOWNLEFT(-1,3),
    HUSARRIGHTDOWN(3,1),
    HUSARDOWNRIGHT(1,3);

    public final int x;
    public final int y;

    Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

package storage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class Icons {

    public static Image getIcon(String name, PlayerColor playerColor) {
        File file = new File(String.format("src/main/resources/%s_%s.png", name.toLowerCase(Locale.ROOT), playerColor.getColorID()));
        BufferedImage image;
        try {
            image = ImageIO.read(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return image.getScaledInstance(Starter.FIELD_LONG, Starter.FIELD_LONG, Image.SCALE_SMOOTH);
    }

    public static ImageIcon getWinnerIcon(PlayerColor playerColor) {
        ImageIcon king;
        king = new ImageIcon(Icons.getIcon("king", playerColor));
        return king;
    }
}

package storage;

public enum PlayerColor {

    WHITE(1),
    BLACK(2);
    private final int colorID;

    PlayerColor(int colorID) {
        this.colorID = colorID;
    }

    public int getColorID() {
        return colorID;
    }



}

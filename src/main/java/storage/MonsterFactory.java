package storage;

import pieces.*;

import java.io.IOException;


public class MonsterFactory {
    private static final String ANGEL = "angel";
    private static final String BISHOP = "bishop";
    private static final String CARDINALE = "cardinale";
    private static final String DRAGON = "dragon";
    private static final String KNIGHT = "knight";
    private static final String ROOK = "rook";



    public static Piece getNewMonster(Piece piece) throws IOException {
        PlayerColor color = piece.getPlayerColor();
        return switch (piece.getPieceName()) {
            case ANGEL -> new Archangel(color);
            case BISHOP -> new Cardinale(color);
            case CARDINALE -> new Pope(color);
            case DRAGON -> new Draco(color);
            case KNIGHT -> new Husar(color);
            case ROOK -> new Titan(color);

            default -> piece;
        };
    }
}

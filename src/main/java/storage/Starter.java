package storage;

import board.Board;
import pieces.*;

import java.awt.*;
import java.io.IOException;

public class Starter {
    public static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    private final PlayerColor player1;
    private final PlayerColor player2;
    public static final int FIELD_LONG = SCREEN_SIZE.height*9/100;
    private final Piece[][] firstSetup = new Piece[Board.LENGTH][Board.LENGTH];

    public Starter(PlayerColor p1, PlayerColor p2) {
        this.player1 = p1;
        this.player2 = p2;
    }

    public void createSetup() throws IOException {
        for (int j = 0; j < firstSetup.length; j++) {
            for (int i = 0; i < firstSetup.length; i++) {
                if (j==2) {
                    firstSetup[i][j] = new Pawn(player2);
                }
                if (j == firstSetup.length-3) {
                    firstSetup[i][j] = new Pawn(player1);
                }

            }
        }

        //Line 1 - player black
        firstSetup[0][0] = new Angel(player2);
        firstSetup[1][0] = new Dragon(player2);
        firstSetup[2][0] = new Knight(player2);
        firstSetup[3][0] = new Hydra(player2);
        firstSetup[4][0] = new Queen(player2);
        firstSetup[5][0] = new King(player2);
        firstSetup[6][0] = new Hydra(player2);
        firstSetup[7][0] = new Knight(player2);
        firstSetup[8][0] = new Dragon(player2);
        firstSetup[9][0] = new Angel(player2);

        //Line 1 - player white
        firstSetup[0][9] = new Angel(player1);
        firstSetup[1][9] = new Dragon(player1);
        firstSetup[2][9] = new Knight(player1);
        firstSetup[3][9] = new Hydra(player1);
        firstSetup[4][9] = new Queen(player1);
        firstSetup[5][9] = new King(player1);
        firstSetup[6][9] = new Hydra(player1);
        firstSetup[7][9] = new Knight(player1);
        firstSetup[8][9] = new Dragon(player1);
        firstSetup[9][9] = new Angel(player1);

        //Line 2 - player black
        firstSetup[0][1] = new Rook(player2);
        firstSetup[1][1] = new Bishop(player2);
        firstSetup[4][1] = new Knight(player2);
        firstSetup[5][1] = new Knight(player2);
        firstSetup[8][1] = new Bishop(player2);
        firstSetup[9][1] = new Rook(player2);

        //Line 2 - player white
        //KNIGHT
        firstSetup[0][8] = new Rook(player1);
        firstSetup[1][8] = new Bishop(player1);
        firstSetup[4][8] = new Knight(player1);
        firstSetup[5][8] = new Knight(player1);
        firstSetup[8][8] = new Bishop(player1);
        firstSetup[9][8] = new Rook(player1);


    }

    public Piece getPiece(int x, int y) {
        return firstSetup[x][y];
    }
}

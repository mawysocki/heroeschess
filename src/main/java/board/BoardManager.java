package board;

import moves.Turn;

import java.awt.*;
import java.util.List;

public class BoardManager {

    public static void highlightMoveFields(List<Field> fields) {
        for (Field field : fields) {
            field.setBackground(Color.GREEN);
        }
    }

    public static void resetHighlights() {
        for (int j = 0; j < Board.LENGTH; j++) {
            for (int i = 0; i < Board.LENGTH; i++) {
                if (Board.getChessBoard()[i][j].isClean())
                    Board.getChessBoard()[i][j].setBackground(Board.getField(i,j).DEFAULT_BACKGROUND);
            }
        }
    }

    public static boolean isOnBoard(int x, int y) {
        return (x >= 0 && y >= 0 && x < 10 && y < 10);
    }

    public static void highlightAttackFields(List<Field> allowedMoves) {
        for (Field field : allowedMoves) {
            if (!field.isEmpty()) {
                if (Turn.getInstance().isOpponent(field)) {
                    field.setBackground(Color.RED);
                } else {
                    field.resetBackground();
                }
            }
        }
    }
}

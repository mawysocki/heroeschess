package board;

import pieces.Piece;
import storage.Obstacle;
import storage.Starter;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

public class Field extends JButton {


    private Piece piece = null;
    private Obstacle obstacle = null;

    private final int indexX;
    private final int indexY;

    public final Color DEFAULT_BACKGROUND;

    public Field(Piece piece, int x, int y, Color backgroundColor, Obstacle obstacle) {
        this.indexX = x;
        this.indexY = y;
        this.setBounds(x, y);
        this.DEFAULT_BACKGROUND = backgroundColor;
        this.setBackground(DEFAULT_BACKGROUND);
        setPiece(piece);
        setObstacle(obstacle);
    }

    public Field(Piece piece, int x, int y, Color backgroundColor) {
        this(piece, x, y, backgroundColor, null);
    }

    public void clearPiece() {
        this.setPiece(null);
    }

    public void clearObstacle() {
        this.setObstacle(null);
    }

    public boolean isEmpty() {
        return piece == null;
    }
    public boolean isClean() {
        return obstacle == null;
    }

    public int getIndexY() {
        return indexY;
    }

    public int getIndexX() {
        return indexX;
    }

    public void setBounds(int x, int y) {
        int startX = 0;
        int startY = 0;
        if (x > 0) {
            startX = x * Starter.FIELD_LONG;
        }
        if (y > 0) {
            startY = y * Starter.FIELD_LONG;
        }
        super.setBounds(startX, startY, Starter.FIELD_LONG, Starter.FIELD_LONG);
    }

    public Piece getPiece() {
        return this.piece;
    }
    public Obstacle getObstacle() {
        return obstacle;
    }
    public void setPiece(Piece p) {
        this.piece = p;
        if (p != null) {
            setImage();
        } else {
            resetImage();
        }
    }

    private void setImage() {
        ImageIcon icon = new ImageIcon(piece.getIcon());
        this.setIcon(icon);
    }

    private void setObstacle(Obstacle obstacle) {
        this.obstacle = obstacle;
        if (obstacle != null) {
            this.setBackground(obstacle.color);
        } else {
            resetBackground();
        }

    }

    private void resetImage() {
        this.setIcon(null);
    }


    public void resetBackground() {
        this.setBackground(DEFAULT_BACKGROUND);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Field)) return false;
        return ((Field) obj).getIndexX() == this.getIndexX() &&
                ((Field) obj).getIndexY() == this.getIndexY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(indexX, indexY);
    }

    @Override
    public String toString() {
        return String.format("X: %s, Y: %s, isempty: %s", indexX, indexY, piece == null);
    }
}

package board;

import moves.PerformManager;
import storage.Starter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;

public class Board extends JFrame {

    private static Board board;
    private static Field[][] chessBoard;
    public static final int LENGTH = 10;
    private final PerformManager moveManager;

    private static boolean isBoardReversed = true;

    private Board(int noOfObstacles) {
        this.setSize(Starter.SCREEN_SIZE);
        this.setLayout(null);
        ChessBoardCreator creator = new ChessBoardCreator(noOfObstacles);
        try {
            chessBoard = creator.createChessBoard(this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        moveManager = new PerformManager();
    }

    public static void newInstance() {
        isBoardReversed = !isBoardReversed;
        board = new Board(0);
    }

    public static void newInstance(int noOfObstacles) {
        isBoardReversed = !isBoardReversed;
        board = new Board(noOfObstacles);
    }

    public static Board getInstance() {
        return board;
    }

    public static boolean isBoardReversed(){
        return isBoardReversed;
    }


    public static Field[][] getChessBoard() {
        return chessBoard;
    }

    public static Field getField(int x, int y) {
        return chessBoard[x][y];
    }


    public void start() {
        this.setVisible(true);
    }

    public void clickField(ActionEvent e) {
        Object source = e.getSource();
        Field clickedField = (Field) source;
        moveManager.performAction(clickedField);
    }

    public void closeBoard() {
        setVisible(false);
        dispose();
    }
}

package board;

import storage.Obstacle;
import storage.PlayerColor;
import storage.Starter;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ChessBoardCreator {
    private Field[][] chessBoard;
    private final int noOfObstacles;

    public ChessBoardCreator(int noOfObstacles) {
        this.noOfObstacles = Math.min(noOfObstacles, Board.LENGTH * 4);
    }

    public Field[][] createChessBoard(Board frame) throws IOException {
        this.chessBoard = new Field[Board.LENGTH][Board.LENGTH];
        createFields(frame);
        return chessBoard;
    }

    public void createFields(Board frame) throws IOException {
        Starter starter;
        if (!Board.isBoardReversed()) {
            starter = new Starter(PlayerColor.WHITE, PlayerColor.BLACK);
        } else {
            starter = new Starter(PlayerColor.BLACK, PlayerColor.WHITE);
        }
        starter.createSetup();
        ObstaclesGenerator generator = new ObstaclesGenerator(noOfObstacles);
        for (int j = 0; j < Board.LENGTH; j++) {
            for (int i = 0; i < Board.LENGTH; i++) {
                Color defaltBackground = (i + j) % 2 != 0 ? Color.PINK : Color.WHITE;
                chessBoard[i][j] = new Field(starter.getPiece(i, j), i, j, defaltBackground, generator.check(i, j));
                setListener(chessBoard[i][j], frame);
                frame.add(chessBoard[i][j]);
            }
        }
    }

    private void setListener(Field button, Board frame) {
        button.addActionListener(frame::clickField);
    }

}

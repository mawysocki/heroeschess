package board;

import storage.Obstacle;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ObstaclesGenerator {

    private List<Point> obstacles;
    private final Random random = new Random();
    public ObstaclesGenerator(int numberOf) {
        generate(numberOf);
    }
    public void generate(int numberOf) {
        obstacles = new ArrayList<>();
        while (obstacles.size() < numberOf) {
            addObstacle();
        }
    }

    private void addObstacle() {
        int x = random.nextInt(Board.LENGTH);
        int y = random.nextInt(7 - 3) + 3;
        Point point = new Point(x, y);
        if (!obstacles.contains(point)) {
            obstacles.add(point);
        }
    }

    public Obstacle check(int x, int y) {
       return obstacles.contains(new Point(x, y)) ? Obstacle.WALL : null;
    }
}

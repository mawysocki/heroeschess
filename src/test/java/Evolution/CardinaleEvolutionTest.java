package Evolution;

import board.Board;
import moves.Turn;
import org.junit.Assert;
import org.junit.Test;
import pieces.*;
import storage.PlayerColor;

import static org.junit.Assert.assertTrue;

public class CardinaleEvolutionTest extends AbstractEvolution {

    @Test
    public void cardinaleEvolutionTest() {
        evolutionTest();
    }

    @Override
    public void prepareMove() {
        new BishopEvolutionTest().bishopEvolutionTest();
        Turn.getInstance().switchTurn();
    }

    @Override
    public void attack() {
        move(6, 3);
        move(7, 2);
    }

    @Override
    public void checkBeforeAttack() {
        Piece piece;
        piece = Board.getField(6, 3).getPiece();
        assertTrue(piece instanceof Cardinale);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);
        piece = Board.getField(1, 8).getPiece();
        Assert.assertNull(piece);
        piece = Board.getField(7, 2).getPiece();
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);
        assertTrue(piece instanceof Pawn);
    }



    @Override
    public void checkAfterAttack() {
        Piece piece;
        piece = Board.getField(6, 3).getPiece();
        Assert.assertNull(piece);
        piece = Board.getField(7, 2).getPiece();
        assertTrue(piece instanceof Pope);
        Assert.assertEquals(Turn.getInstance().getCurrentTurn(), PlayerColor.BLACK);
    }
}

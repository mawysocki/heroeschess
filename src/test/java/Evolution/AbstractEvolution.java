package Evolution;

import Hooks.Hook;

public abstract class AbstractEvolution extends Hook {

    public void evolutionTest() {
        prepareMove();
        checkBeforeAttack();
        attack();
        checkAfterAttack();
    }
    public abstract void prepareMove();
    public abstract void checkBeforeAttack();

    public abstract void attack();
    public abstract void checkAfterAttack();
}

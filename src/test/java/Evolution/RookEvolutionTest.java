package Evolution;

import Hooks.Hook;
import board.Board;
import moves.Turn;
import org.junit.Assert;
import org.junit.Test;
import pieces.Pawn;
import pieces.Piece;
import pieces.Rook;
import pieces.Titan;
import storage.PlayerColor;

import static org.junit.Assert.assertTrue;

public class RookEvolutionTest extends AbstractEvolution {

    @Test
    public void rookEvolutionTest() {
        evolutionTest();
    }

    @Override
    public void prepareMove() {
        move(0, 7);
        move(1, 6);
        move(0, 2);
        move(0, 3);
        move(0, 8);
        move(0, 5);
        move(0, 3);
        move(0, 4);
    }

    @Override
    public void checkBeforeAttack() {
        Piece piece;
        piece = Board.getField(0, 5).getPiece();
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);
        assertTrue(piece instanceof Rook);
        piece = Board.getField(0, 4).getPiece();
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);
        assertTrue(piece instanceof Pawn);
        Assert.assertEquals(Turn.getInstance().getCurrentTurn(), PlayerColor.WHITE);
    }

    @Override
    public void attack() {
        move(0, 5);
        move(0, 4);
    }

    @Override
    public void checkAfterAttack() {
        Piece piece;
        piece = Board.getField(0, 5).getPiece();
        Assert.assertNull(piece);
        piece = Board.getField(0, 4).getPiece();
        assertTrue(piece instanceof Titan);
        Assert.assertEquals(Turn.getInstance().getCurrentTurn(), PlayerColor.BLACK);
    }
}

package Evolution;

import board.Board;
import moves.Turn;
import org.junit.Assert;
import org.junit.Test;
import pieces.*;
import storage.PlayerColor;

import static org.junit.Assert.assertTrue;

public class BishopEvolutionTest extends AbstractEvolution {

    @Test
    public void bishopEvolutionTest() {
        evolutionTest();
    }

    @Override
    public void prepareMove() {
        move(2,7);
        move(2,6);
        move(6, 2);
        move(6, 3);
        move(1, 8);
        move(5, 4);
        Turn.getInstance().switchTurn();
    }

    @Override
    public void checkBeforeAttack() {
        Piece piece;
        piece = Board.getField(5, 4).getPiece();
        assertTrue(piece instanceof Bishop);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);
        piece = Board.getField(1, 8).getPiece();
        Assert.assertNull(piece);
        piece = Board.getField(6, 3).getPiece();
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);
        assertTrue(piece instanceof Pawn);
    }

    @Override
    public void attack() {
        move(5, 4);
        move(6, 3);
    }

    @Override
    public void checkAfterAttack() {
        Piece piece;
        piece = Board.getField(5, 4).getPiece();
        Assert.assertNull(piece);
        piece = Board.getField(6, 3).getPiece();
        assertTrue(piece instanceof Cardinale);
        Assert.assertEquals(Turn.getInstance().getCurrentTurn(), PlayerColor.BLACK);
    }
}

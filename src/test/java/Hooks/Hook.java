package Hooks;

import board.Board;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;

public class Hook extends AbstractMove{
    @Before
    public void runGame() {
        Board.newInstance();
        Board.getInstance().start();
    }

    @After
    public void watch() throws InterruptedException {
        Thread.sleep(3000);
    }

}

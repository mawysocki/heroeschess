package Hooks;

import board.Board;

public abstract class AbstractMove {
    public void move(int x, int y) {
        Board.getChessBoard()[x][y].doClick();
    }
}

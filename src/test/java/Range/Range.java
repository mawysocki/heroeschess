package Range;

public interface Range {

    void preparePlace();
    void moveTooFar();
    void checkTooFar();
    void moveMax();
    void checkMax();
}

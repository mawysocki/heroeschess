package Range;

import Hooks.Hook;
import org.junit.After;
import org.junit.Test;


public class RangeTest extends Hook {
    @After
    public void waitFor() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    private void executeTest(Range range) {
        range.preparePlace();
        range.moveTooFar();
        range.checkTooFar();
        range.moveMax();
        range.checkMax();
    }
    @Test
    public void pawnRangeTest() {
        executeTest(new PawnRange());
    }

    @Test
    public void bishopRangeTest() {
        executeTest(new BishopRange());
    }

    @Test
    public void dragonRangeTest() {
        executeTest(new DragonRange());
    }
    @Test
    public void knightRangeTest() {
        executeTest(new KnightRange());
    }
    @Test
    public void rookRangeTest() {
        executeTest(new RookRange());
    }

}

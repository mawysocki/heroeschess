package Range;

import board.Board;
import moves.Turn;
import org.junit.Assert;
import pieces.Bishop;
import pieces.Dragon;
import pieces.Piece;

import java.awt.*;

import static org.junit.Assert.assertTrue;

public class BishopRange extends AbstractRange {
    public BishopRange() {
        startX = 1;
        startY = 8;
        maxX = 5;
        maxY = 4;
        tooFarX = 6;
        tooFarY = 3;
        background = Color.PINK;
    }
    @Override
    public void preparePlace() {
        move(2,7);
        move(2,6);
        Turn.getInstance().switchTurn();
    }

    @Override
    public void moveTooFar() {
        moveTooFar(startX, startY, tooFarX, tooFarY);
    }

    @Override
    public void checkTooFar() {
        checkTooFar(tooFarX, tooFarY);

        Piece piece = Board.getField(startX, startY).getPiece();
        assertTrue(piece instanceof Bishop);
        Assert.assertEquals(Board.getField(startX, startY).getBackground(), Color.ORANGE);
    }

    @Override
    public void moveMax() {
        moveMax(maxX, maxY);
    }

    @Override
    public void checkMax() {
        checkMax(startX, startY, background);

        Piece piece = Board.getField(maxX, maxY).getPiece();
        assertTrue(piece instanceof Bishop);
    }
}

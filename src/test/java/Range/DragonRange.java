package Range;

import board.Board;
import moves.Turn;
import org.junit.Assert;
import pieces.Dragon;
import pieces.Piece;

import java.awt.*;

import static org.junit.Assert.assertTrue;

public class DragonRange extends AbstractRange {
    public DragonRange() {
        startX = 1;
        startY = 9;
        maxX = 5;
        maxY = 5;
        tooFarX = 6;
        tooFarY = 4;
    }

    @Override
    public void preparePlace() {
        Board.getChessBoard()[3][7].doClick();
        Board.getChessBoard()[3][6].doClick();
        Turn.getInstance().switchTurn();
    }

    @Override
    public void moveTooFar() {
        moveTooFar(startX, startY, tooFarX, tooFarY);
    }

    @Override
    public void checkTooFar() {
        checkTooFar(tooFarX, tooFarY);

        Piece piece = Board.getField(startX, startY).getPiece();
        assertTrue(piece instanceof Dragon);
        Assert.assertEquals(Board.getField(startX, startY).getBackground(), Color.ORANGE);
    }

    @Override
    public void moveMax() {
        moveMax(maxX, maxY);
    }

    @Override
    public void checkMax() {
        checkMax(startX, startY, background);

        Piece piece = Board.getField(maxX, maxY).getPiece();
        assertTrue(piece instanceof Dragon);

    }
}

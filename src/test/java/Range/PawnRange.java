package Range;

import board.Board;
import org.junit.Assert;
import pieces.Pawn;
import pieces.Piece;

import java.awt.*;

import static org.junit.Assert.assertTrue;

public class PawnRange extends AbstractRange {
    public PawnRange() {
        startX = 0;
        startY = 7;
        maxX = 0;
        maxY = 6;
        tooFarX = 0;
        tooFarY = 5;
        background = Color.PINK;
    }

    @Override
    public void preparePlace() {

    }

    @Override
    public void moveTooFar() {
        moveTooFar(startX, startY, tooFarX, tooFarY);
    }
    @Override
    public void checkTooFar() {
        checkTooFar(tooFarX, tooFarY);

        Piece piece;
        piece = Board.getField(startX, startY).getPiece();
        assertTrue(piece instanceof Pawn);
        Assert.assertEquals(Board.getField(startX, startY).getBackground(), Color.ORANGE);
    }
    @Override
    public void moveMax() {
        moveMax(maxX,maxY);
    }
    @Override
    public void checkMax() {
        checkMax(startX, startY, background);

        Piece piece = Board.getField(maxX, maxY).getPiece();
        assertTrue(piece instanceof Pawn);

    }
}

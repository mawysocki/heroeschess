package Range;

import board.Board;
import moves.Turn;
import org.junit.Assert;
import pieces.Piece;
import pieces.Rook;

import java.awt.*;

import static org.junit.Assert.assertTrue;

public class RookRange extends AbstractRange {
    public RookRange() {
        startX = 0;
        startY = 8;
        maxX = 0;
        maxY = 4;
        tooFarX = 0;
        tooFarY = 3;
    }

    @Override
    public void preparePlace() {
        Board.getChessBoard()[0][7].doClick();
        Board.getChessBoard()[1][6].doClick();
        Turn.getInstance().switchTurn();
    }

    @Override
    public void moveTooFar() {
        moveTooFar(startX, startY, tooFarX, tooFarY);
    }
    @Override
    public void checkTooFar() {
        checkTooFar(tooFarX, tooFarY);

        Piece piece;
        piece = Board.getField(startX, startY).getPiece();
        assertTrue(piece instanceof Rook);
        Assert.assertEquals(Board.getField(startX, startY).getBackground(), Color.ORANGE);
    }
    @Override
    public void moveMax() {
        moveMax(maxX,maxY);
    }
    @Override
    public void checkMax() {
        checkMax(startX, startY, background);

        Piece piece = Board.getField(maxX, maxY).getPiece();
        assertTrue(piece instanceof Rook);

    }
}

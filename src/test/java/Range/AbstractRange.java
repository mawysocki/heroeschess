package Range;

import Hooks.AbstractMove;
import board.Board;
import board.Field;
import moves.Turn;
import org.junit.Assert;
import pieces.Piece;
import storage.PlayerColor;

import java.awt.*;

public abstract class AbstractRange extends AbstractMove implements Range{

    protected int startX;
    protected int startY;
    protected int maxX;
    protected int maxY;
    protected int tooFarX;
    protected int tooFarY;

    protected Color background = Color.WHITE;

    protected void moveTooFar(int x0, int y0, int x1, int y1) {
        Board.getChessBoard()[x0][y0].doClick();
        Board.getChessBoard()[x1][y1].doClick();
    }
    protected void moveMax(int x, int y) {
        Board.getChessBoard()[x][y].doClick();}

    protected void checkTooFar(int x, int y) {
        Piece piece;
        piece = Board.getField(x, y).getPiece();
        Assert.assertNull(piece);
        Assert.assertEquals(Turn.getInstance().getCurrentTurn(), PlayerColor.WHITE);
    }

    protected void checkMax(int x, int y, Color background) {        Piece piece;
        Field field = Board.getField(x, y);
        piece = field.getPiece();
        Assert.assertNull(piece);
        Assert.assertEquals(background, field.getBackground());
        Assert.assertEquals(Turn.getInstance().getCurrentTurn(), PlayerColor.BLACK);}
}

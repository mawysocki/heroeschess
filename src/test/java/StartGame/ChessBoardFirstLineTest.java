package StartGame;

import Hooks.Hook;
import board.Board;
import org.junit.Assert;
import org.junit.Test;
import pieces.*;
import storage.PlayerColor;

import static org.junit.Assert.assertTrue;

public class ChessBoardFirstLineTest extends Hook {
    @Test
    public void chessBoardContentFirstLineTest() {
        checkAngel();
        checkDragon();
        checkKnight();
        checkHydra();
        checkKings();
    }

    public void checkAngel() {
        Piece piece;
        piece = Board.getField(0, 0).getPiece();
        assertTrue(piece instanceof Angel);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

        piece = Board.getField(0, 9).getPiece();
        assertTrue(piece instanceof Angel);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);

        piece = Board.getField(9, 0).getPiece();
        assertTrue(piece instanceof Angel);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

        piece = Board.getField(9, 9).getPiece();
        assertTrue(piece instanceof Angel);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);
    }

    public void checkDragon() {
        Piece piece;
        piece = Board.getField(1, 0).getPiece();
        assertTrue(piece instanceof Dragon);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

        piece = Board.getField(1, 9).getPiece();
        assertTrue(piece instanceof Dragon);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);

        piece = Board.getField(8, 0).getPiece();
        assertTrue(piece instanceof Dragon);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

        piece = Board.getField(8, 9).getPiece();
        assertTrue(piece instanceof Dragon);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);
    }

    public void checkKnight() {
        Piece piece;
        piece = Board.getField(2, 0).getPiece();
        assertTrue(piece instanceof Knight);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

        piece = Board.getField(2, 9).getPiece();
        assertTrue(piece instanceof Knight);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);

        piece = Board.getField(7, 0).getPiece();
        assertTrue(piece instanceof Knight);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

        piece = Board.getField(7, 9).getPiece();
        assertTrue(piece instanceof Knight);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);
    }

    public void checkHydra() {
        Piece piece;
        piece = Board.getField(3, 0).getPiece();
        assertTrue(piece instanceof Hydra);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

        piece = Board.getField(3, 9).getPiece();
        assertTrue(piece instanceof Hydra);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);

        piece = Board.getField(6, 0).getPiece();
        assertTrue(piece instanceof Hydra);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

        piece = Board.getField(6, 9).getPiece();
        assertTrue(piece instanceof Hydra);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);
    }

    public void checkKings() {
        Piece piece;
        piece = Board.getField(4, 0).getPiece();
        assertTrue(piece instanceof Queen);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

        piece = Board.getField(5, 9).getPiece();
        assertTrue(piece instanceof King);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);

        piece = Board.getField(4, 0).getPiece();
        assertTrue(piece instanceof Queen);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

        piece = Board.getField(5, 9).getPiece();
        assertTrue(piece instanceof King);
        Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);
    }

}

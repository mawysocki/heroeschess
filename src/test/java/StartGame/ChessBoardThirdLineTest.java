package StartGame;

import Hooks.Hook;
import board.Board;
import org.junit.Assert;
import org.junit.Test;
import pieces.Pawn;
import pieces.Piece;
import storage.PlayerColor;

import static org.junit.Assert.assertTrue;

public class ChessBoardThirdLineTest extends Hook {
    @Test
    public void chessBoardContentPawnTest() {
        Piece piece;
        for (int i = 0; i < Board.LENGTH; i++) {
            piece = Board.getField(i, 2).getPiece();
            assertTrue(piece instanceof Pawn);
            Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);

            piece = Board.getField(i, 7).getPiece();
            assertTrue(piece instanceof Pawn);
            Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);
        }
    }

    @Test
    public void chessBoardContentPawnTest2() {
        Piece piece;
        for (int i = 0; i < Board.LENGTH; i++) {
            piece = Board.getField(i, 2).getPiece();
            assertTrue(piece instanceof Pawn);
            Assert.assertEquals(piece.getPlayerColor(), PlayerColor.WHITE);

            piece = Board.getField(i, 7).getPiece();
            assertTrue(piece instanceof Pawn);
            Assert.assertEquals(piece.getPlayerColor(), PlayerColor.BLACK);
        }
    }

}

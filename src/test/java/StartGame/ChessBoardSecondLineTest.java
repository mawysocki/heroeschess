package StartGame;

import Hooks.Hook;
import board.Board;
import org.junit.Assert;
import org.junit.Test;
import pieces.*;
import storage.PlayerColor;

import static org.junit.Assert.assertTrue;

public class ChessBoardSecondLineTest extends Hook {
    @Test
    public void chessBoardContentSecondLineTest() {
        Piece piece;
        for (int i = 0; i < Board.LENGTH; i++) {
            piece = Board.getField(i, 1).getPiece();
            Assert.assertNotNull(piece);
            piece = Board.getField(i, 8).getPiece();
            Assert.assertNotNull(piece);
        }
    }



}

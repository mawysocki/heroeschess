package Moves;

import Hooks.Hook;
import board.Board;
import moves.Turn;
import org.junit.Assert;
import org.junit.Test;
import pieces.Angel;
import pieces.Pawn;
import pieces.Piece;
import storage.PlayerColor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SingleMoveTest extends Hook {

    @Test
    public void movePawn() {
        checkBefore();
        move(0,7);
        move(1,6);
        checkAfter();
    }

    public void checkBefore() {
        Piece piece;
        piece = Board.getField(0, 7).getPiece();
        assertTrue(piece instanceof Pawn);
        piece = Board.getField(1, 6).getPiece();
        Assert.assertNull(piece);
        Assert.assertEquals(Turn.getInstance().getCurrentTurn(), PlayerColor.WHITE);
    }

    public void checkAfter() {
        Piece piece;
        piece = Board.getField(0, 7).getPiece();
        Assert.assertNull(piece);
        piece = Board.getField(1, 6).getPiece();
        assertTrue(piece instanceof Pawn);
        Assert.assertEquals(Turn.getInstance().getCurrentTurn(), PlayerColor.BLACK);
    }
}
